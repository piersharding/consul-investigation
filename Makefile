# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c
BASE = $(shell pwd)

#########################################
# References for Consul
# https://jerome-ran.medium.com/my-raw-notes-on-consul-beginners-guide-11ada1558dc1


#
CONSUL_IMAGE ?= hashicorp/consul:1.17


consul-network: # reconfigure consul network to /16
	docker rm -f $(DC)-$(NODE) || true
	docker network rm consul || true
	docker network create --subnet 172.19.0.0/24 --driver bridge consul

DC ?= skao
CONSUL_HOST ?= $(DC)-node3
NODE_LIST ?= node1:172.19.0.3 node2:172.19.0.4 node3:172.19.0.5
EXPECT_NODES ?= 3
LEADER ?= node1
LEADER_IP ?= 172.19.0.3
UI_IP ?= 172.19.0.10
NODE ?= $(LEADER)
$(eval NODES := $(shell for i in $(NODE_LIST); do echo "$${i}" | cut -d ':' -f 1; done))
$(eval NODE_ADDR := $(shell for i in $(NODE_LIST); do echo "$${i}" | cut -d ':' -f 2; done))
FOLLOWER_LIST=$(shell echo $(NODE_LIST) | cut -d ' ' -f 2- )
FOLLOWERS=$(shell echo $(NODES) | cut -d ' ' -f 2- )
FOLLOWER_IPS=$(shell echo $(NODE_ADDR) | cut -d ' ' -f 2- )
$(eval HOST_LIST := --add-host wattle.local.net:192.168.110.77 $(shell for i in $(NODE_LIST); do echo -n "--add-host $${i} "; done))
CONTAINER_IP ?= 172.19.0.3
EXPOSE_PORTS ?= -p 127.0.0.1:8500:8500 -p 8502:8502 -p 8503:8503 \
	-p 8600:8600 -p 8600:8600/udp \
	-p 8301:8301 -p 8302:8302
TYPE ?= -server
EXTRA_ARGS ?=
MASTER_TOKEN ?= 9a6c723f-2533-2679-4515-654cdb7f96c9
AGENT_TOKEN ?= 7a59f860-7e6a-0037-52d6-270ee84e4bed
UI_AGENT_TOKEN ?= 7a59f860-7e6a-0037-52d6-270ee84e4bad
REPLICATION_TOKEN ?= FA294BBA-8279-4F93-84A2-FCBA61C8298A
DEFAULT_TOKEN ?= 7a59f860-7e6a-0037-52d6-270ebeefdead
UI_DEFAULT_TOKEN ?= 7a59f860-7e6a-0037-52d6-270ebeefdaed
DNS_TOKEN ?= 7a59f860-7e6a-0037-52d6-270edeadbeef

REDIS_NAME ?= redis
REDIS_IMAGE ?= redis:7.0.8
REDIS_IP ?= 172.19.0.51

OAUTH2PROXY_NAME ?= oauth2proxy
OAUTH2PROXY_IMAGE ?= quay.io/oauth2-proxy/oauth2-proxy:v7.2.1
OAUTH2PROXY_IP ?= 172.19.0.52
OAUTH2PROXY_CLIENT_ID ?= id
OAUTH2PROXY_CLIENT_SECRET ?= secret
OAUTH2PROXY_COOKIE_SECRET ?= secret

HAPROXY_NAME ?= haproxy
HAPROXY_IMAGE ?= docker.io/haproxy:2.8
HAPROXY_IP ?= 172.19.0.53
UI_HOST ?= wattle.local.net

# define overides for above variables in here
-include PrivateRules.mak

# multi-DC
# https://github.com/dj-wasabi/consul-acl-examples/tree/master/multiple-dc


##########################################################
# https://developer.hashicorp.com/consul/docs/services/discovery/dns-static-lookups

define CLUSTER_CONFIG

{
	"data_dir": "/consul/data",
	"log_level": "INFO",
	"client_addr": "0.0.0.0",
	"ui_config": {
      "enabled": true
	},
	"server": true,
	"bootstrap_expect": $(EXPECT_NODES),
	"disable_update_check": true,
	"enable_acl_replication": true,
	"primary_datacenter": "$(DC)",
	"datacenter": "$(DC)",
	"alt_domain": "$(DC)",
	"acl": {
		"enabled": true,
		"default_policy": "deny",
		"down_policy": "extend-cache",
		"enable_token_replication": true,
		"enable_token_persistence": true,
		"tokens": {
			"agent": "$(AGENT_TOKEN)",
			"initial_management": "$(MASTER_TOKEN)",
			"replication": "$(REPLICATION_TOKEN)",
			"default": "$(DEFAULT_TOKEN)"
		}
	},
	"retry_join": [ "node1", "node2", "node3" ]
}

endef

export CLUSTER_CONFIG


##########################################################
# https://developer.hashicorp.com/consul/docs/services/discovery/dns-static-lookups
# https://developer.hashicorp.com/consul/docs/services/configuration/services-configuration-reference#configuration-model

define SERVICE1_CONFIG
{
  "service": {
    "name": "tangoexample1",
    "tags": [
      "tango"
    ],
    "port": 45050,
    "address": "10.10.1.1"
  }
}

endef

export SERVICE1_CONFIG


vars: set-config
	@echo "NODE_LIST=$(NODE_LIST)"
	@echo "NODES=$(NODES)"
	@echo "NODE_ADDR=$(NODE_ADDR)"
	@echo "HOST_LIST=$(HOST_LIST)"
	@echo "FOLLOWER_LIST=$(FOLLOWER_LIST)"
	@echo "FOLLOWERS=$(FOLLOWERS)"
	@echo "FOLLOWER_IPS=$(FOLLOWER_IPS)"
	@echo "LEADER=$(LEADER)"
	@echo "CLUSTER_CONFIG="
	@cat $(BASE)/config/$(DC)-$(NODE)/cluster.json

set-config:
	mkdir -p $(BASE)/config/$(DC)-$(NODE)
	echo "$${CLUSTER_CONFIG}" > $(BASE)/config/$(DC)-$(NODE)/cluster.json

set-service-config:
	echo "$${SERVICE1_CONFIG}" > $(BASE)/bootstrap/service1.json

consul-node: set-config set-service-config
	docker rm -f $(DC)-$(NODE) || true
	sleep 1
	mkdir -p $(BASE)/data/$(DC)-$(NODE)
	mkdir -p $(BASE)/config/$(DC)-$(NODE)
	docker run --network consul --name $(DC)-$(NODE) \
	$(HOST_LIST) \
	$(EXPOSE_PORTS) \
	--ip $(CONTAINER_IP) \
	-v $(BASE)/data/$(DC)-$(NODE):/consul/data \
	-v $(BASE)/config/$(DC)-$(NODE):/consul/config \
	-v $(BASE)/bootstrap:/tmp/config \
	-e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) \
	-d $(CONSUL_IMAGE) agent -datacenter $(DC) -node $(NODE) \
	-bind 0.0.0.0 -client 0.0.0.0 -advertise $(CONTAINER_IP) $(TYPE) \
	$(EXTRA_ARGS)

node-stop:
	docker exec -i -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) $(DC)-$(NODE) \
	sh -c "consul leave" || true
	sleep 1
	docker rm -f $(DC)-$(NODE) || true
	sudo rm -rf $(BASE)/data/$(DC)-$(NODE)
	rm -rf $(BASE)/config/$(DC)-$(NODE)

leader:
	curl -s $(LEADER_IP):8500/v1/status/leader
	CONSUL_LEADER=`docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) sh -c "consul operator raft list-peers" | grep $(LEADER) | grep leader`; \
	echo "CONSUL_LEADER=$${CONSUL_LEADER}"

set-agent-token:
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(DC)-$(NODE)" consul acl set-agent-token agent "$(AGENT_TOKEN)"

cluster:
	make consul-node NODE=$(LEADER) \
	CONTAINER_IP=$(LEADER_IP) EXTRA_ARGS=" -bootstrap-expect $(EXPECT_NODES)"
	$(foreach node,$(FOLLOWER_LIST), make consul-node NODE=$(shell echo $(node) | cut -d ':' -f 1) CONTAINER_IP=$(shell echo $(node) | cut -d ':' -f 2) EXTRA_ARGS="-join $(LEADER_IP)" EXPOSE_PORTS="";)

	sleep 2
	CONSUL_LEADER=`curl -s $(LEADER_IP):8500/v1/status/leader`; \
	while [[ "$${CONSUL_LEADER}" == "\"\"" ]];do \
		sleep 1; \
		CONSUL_LEADER=`curl -s $(LEADER_IP):8500/v1/status/leader`; \
	done

	echo "$$(date +"%Y-%m-%d %H:%M:%S"): Configure Agent ACL"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl policy create -name agent-token -rules @/tmp/config/agent.hcl
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl token create -description "agent token" -policy-name agent-token -secret="$(AGENT_TOKEN)"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl set-agent-token agent "$(AGENT_TOKEN)"
	$(foreach node,$(NODES), make set-agent-token NODE=$(shell echo $(node) | cut -d ':' -f 1) ;)

	@echo "$$(date +"%Y-%m-%d %H:%M:%S"): Configure DNS ACL"
	# shellcheck disable=SC2086
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -it $(CONSUL_HOST) consul acl policy create -name "dns-requests" -rules @/tmp/config/dns.hcl
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl token create -description "Token for DNS Requests" -policy-name dns-requests -secret="$(DNS_TOKEN)"

	@echo "$$(date +"%Y-%m-%d %H:%M:%S"): Configure Replication ACL"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl policy create -name replication -rules @/tmp/config/replication.hcl
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl token create -description "replication token" -policy-name replication -secret="$(REPLICATION_TOKEN)"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl set-agent-token replication "$(REPLICATION_TOKEN)"

	@echo "$$(date +"%Y-%m-%d %H:%M:%S"): Configure Default ACL"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl policy create -name default -rules @/tmp/config/default.hcl
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl token create -description "Default token" -policy-name default -secret="$(DEFAULT_TOKEN)"
	for i in $(NODES); do \
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(DC)-$${i}" consul acl set-agent-token default "$(DEFAULT_TOKEN)"; \
	done

	@echo "$$(date +"%Y-%m-%d %H:%M:%S"): Configure Anon ACL"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl policy create -name anon -rules @/tmp/config/anon.hcl
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl token create -description "Anonymous token" -policy-name anon -secret="$(ANON_TOKEN)"

	@echo "$$(date +"%Y-%m-%d %H:%M:%S"): Consul Cluster configured completely"



define CLUSTER_CONFIG_UI

{
	"data_dir": "/consul/data",
	"log_level": "INFO",
	"client_addr": "0.0.0.0",
	"ui_config": {
      "enabled": true
	},
	"disable_update_check": true,
	"enable_acl_replication": true,
	"primary_datacenter": "$(DC)",
	"datacenter": "$(DC)",
	"alt_domain": "$(DC)",
	"acl": {
		"enabled": true,
		"default_policy": "deny",
		"down_policy": "extend-cache",
		"enable_token_replication": true,
		"enable_token_persistence": true,
		"tokens": {
			"default": "$(DEFAULT_TOKEN)"
		}
	}
}

endef

export CLUSTER_CONFIG_UI


ui:
	docker rm -f $(DC)-ui || true
	sleep 1
	mkdir -p $(BASE)/config/$(DC)-ui
	echo "$${CLUSTER_CONFIG_UI}" > $(BASE)/config/$(DC)-ui/cluster.json
	mkdir -p $(BASE)/data/$(DC)-ui
	docker run --network consul --name $(DC)-ui \
	$(HOST_LIST) \
	 -p 8555:8500 \
	--ip $(UI_IP) \
	-v $(BASE)/data/$(DC)-ui:/consul/data \
	-v $(BASE)/config/$(DC)-ui:/consul/config \
	-d $(CONSUL_IMAGE) agent -datacenter $(DC) -node ui \
	-bind 0.0.0.0 -client 0.0.0.0 -advertise $(UI_IP) -ui \
	-read-replica -allow-write-http-from=127.0.0.1/32 \
	-allow-write-http-from=172.19.0.0/24 \
	$(EXTRA_ARGS) -join $(LEADER_IP)
	sleep 3

	# set agent specific tokens for UI node
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(DC)-ui" \
	consul acl token create -description "$(DC)-ui agent token" \
  	-node-identity "ui:$(DC)" -secret="$(UI_AGENT_TOKEN)" -policy-name agent-token
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(DC)-ui" consul acl set-agent-token agent "$(UI_AGENT_TOKEN)"
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul acl policy create -name ui-default -rules @/tmp/config/ui-default.hcl
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(DC)-ui" \
	consul acl token create -description "$(DC)-ui default token" \
  	-node-identity "ui:$(DC)" -secret="$(UI_DEFAULT_TOKEN)" -policy-name ui-default
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(DC)-ui" consul acl set-agent-token default "$(UI_DEFAULT_TOKEN)"

clean-ui:
	make node-stop NODE=ui

service:
	docker exec -e CONSUL_HTTP_TOKEN="$(MASTER_TOKEN)" -it "$(CONSUL_HOST)" consul services register /tmp/config/service1.json

all: cluster set-leader service members leader kvput proxy

clean: clean-proxy clean-ui
	$(foreach node,$(NODES), make node-stop NODE=$(shell echo $(node) | cut -d ':' -f 1) ;)

members:
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
	sh -c "consul members"
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
	sh -c "consul operator raft list-peers"

kvput:
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
	sh -c "consul kv put my/config/testvalue Wahoooo"

kvget:
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
	sh -c "consul kv get my/config/testvalue"

set-leader:
	CONSUL_LEADER=`docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) sh -c "consul operator raft list-peers" | grep $(LEADER) | grep leader`; \
	echo "CONSUL_LEADER=$${CONSUL_LEADER}"; \
	while [[ "$${CONSUL_LEADER}" == "" ]];do \
		docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
		sh -c "consul operator raft transfer-leader"; \
		sleep 1; \
		CONSUL_LEADER=`docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
		sh -c "consul operator raft list-peers" | grep $(LEADER) | grep leader`; \
		echo "CONSUL_LEADER=$${CONSUL_LEADER}"; \
	done
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -i $(DC)-$(LEADER) \
	sh -c "consul operator raft list-peers"

consul:
	docker exec -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -ti $(DC)-$(LEADER) \
	sh

prompt:
	docker run --rm -e CONSUL_HTTP_TOKEN=$(MASTER_TOKEN) -ti $(CONSUL_IMAGE) \
	sh

dns-example:
	for i in $(NODE_ADDR); do \
	dig @$${i} -p 8600 tangoexample1.service.skao ANY  +noall +answer; \
	dig @$${i} -p 8600 tangoexample1.service.consul ANY  +noall +answer; \
	done


##########################################################
# START OF HAProxy config
# essentially create a heredoc of the HAProxy config file
# CAUTION!: variables get interpolated immediately
# https://github.com/diptadas/probook/blob/master/oauth2/oauth2-haproxy.md
##########################################################
define HAPROXY_CONFIG

#############################
#  ui-proxy config
#############################
global
  log 127.0.0.1 local0
  log 127.0.0.1 local1 notice
  maxconn 4096
  tune.ssl.default-dh-param 2048
  ssl-default-bind-ciphers ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:!kEDH:!LOW:!EXP:!MD5:!aNULL:!eNULL
  ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets
  # https://github.com/rxi/json.lua/blob/master/json.lua
  # https://github.com/haproxytech/haproxy-lua-http
  # https://github.com/TimWolla/haproxy-auth-request/blob/main/README.md
  lua-prepend-path /usr/share/haproxy/?/http.lua
  lua-load /usr/local/etc/haproxy/auth-request.lua

defaults
  log   global
  mode  http
  option    httplog
  option    dontlognull
  timeout connect 5s
  timeout client 1800s
  timeout client-fin 50s
  timeout server 1800s
  timeout tunnel 1h

frontend stats
  bind *:9000
  mode http
  stats enable
  stats hide-version
  stats realm Haproxy\ Statistics
  stats uri /haproxy_stats
  stats auth user:password
  stats refresh 10s
  stats admin if LOCALHOST

frontend front-ui-http-proxy
  bind *:80
  mode http
  option httplog
  #default_backend back-ui-http-proxy

  acl acl_host hdr(host) -i $(UI_HOST)
  acl acl_host hdr(host) -i $(UI_HOST):80

  acl acl_path_oauth2 path_beg /oauth2
  use_backend auth_request if acl_path_oauth2 acl_host

  use_backend back-ui-http-proxy if acl_host

  timeout client 1800s
  timeout client-fin 50s

backend auth_request
        server auth_request $(OAUTH2PROXY_IP):4180


# https://localhost:8500/v1/internal/acl/authorize?dc=skao
#   http-request redirect location /oauth2/start?rd=https://$(UI_HOST)/v1/acl/token/self?dc=skao if ! { var(txn.auth_response_successful) -m bool }
#   http-request add-header Authorization Bearer\ $(MASTER_TOKEN)
#   http-request add-header X-Consul-Token $(MASTER_TOKEN)
# https://wattle.local.net/v1/acl/token/self?dc=skao
# https://wattle.local.net/v1/internal/acl/authorize?dc=skao

backend back-ui-http-proxy
  mode http
  option http-server-close
  balance roundrobin
  http-request lua.auth-request auth_request /oauth2/auth
  http-request redirect location /oauth2/start?rd=https://$(UI_HOST)/v1/internal/acl/authorize?dc=skao if ! { var(txn.auth_response_successful) -m bool }
  redirect scheme https if !{ ssl_fc }
  server server1 $(LEADER_IP):8500 check
  timeout connect 10s
  timeout server 1800s

frontend front-ui-https-proxy
  bind *:443 ssl crt /usr/local/etc/haproxy/server.pem
  http-request add-header X-Forwarded-Proto https
  http-request add-header X-Consul-Token $(MASTER_TOKEN)
  mode http
  option forwardfor
  option http-server-close
  option httplog
  maxconn 2048
  #default_backend back-ui-http-proxy

  acl acl_host hdr(host) -i $(UI_HOST)
  acl acl_host hdr(host) -i $(UI_HOST):80

  acl acl_path_oauth2 path_beg /oauth2
  use_backend auth_request if acl_path_oauth2 acl_host

  use_backend back-ui-http-proxy if acl_host

  timeout client 1800s
  timeout client-fin 50s


endef
export HAPROXY_CONFIG

redis:
	docker rm -f $(REDIS_NAME) || true
	sleep 1
	docker run --network consul --name $(REDIS_NAME) \
	$(HOST_LIST) \
	-p 6379:6379 \
	--ip $(REDIS_IP) \
	-d $(REDIS_IMAGE) redis-server --port 6379 --loglevel warning

oauth2proxy:
	docker rm -f $(OAUTH2PROXY_NAME) || true
	sleep 1
	docker run --network consul --name $(OAUTH2PROXY_NAME) \
	$(HOST_LIST) \
	-p 4180:4180 \
	--ip $(OAUTH2PROXY_IP) \
 	-e OAUTH2_PROXY_CLIENT_ID="$(OAUTH2PROXY_CLIENT_ID)" \
 	-e OAUTH2_PROXY_CLIENT_SECRET="$(OAUTH2PROXY_CLIENT_SECRET)" \
 	-e OAUTH2_PROXY_COOKIE_SECRET="$(OAUTH2PROXY_COOKIE_SECRET)" \
	-d $(OAUTH2PROXY_IMAGE) /bin/oauth2-proxy \
	--provider=gitlab \
	--client-id=$(OAUTH2PROXY_CLIENT_ID) \
	--client-secret=$(OAUTH2PROXY_CLIENT_SECRET) \
	--gitlab-group=ska-telescope \
	--upstream=file:///dev/null \
	--http-address=0.0.0.0:4180 \
	--cookie-secure=true \
	--redirect-url=https://$(UI_HOST)/oauth2/callback \
	--skip-provider-button=true \
	--set-xauthrequest=true \
	--skip-auth-preflight=false \
	--email-domain=* \
	--pass-access-token \
	--pass-authorization-header \
	--session-store-type=redis \
	--redis-connection-url=redis://$(REDIS_IP):6379

self-signed:
	mkdir -p $(BASE)/config/$(HAPROXY_NAME)
	if [ ! -f $(BASE)/config/$(HAPROXY_NAME)/server.key ]; then \
		openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
		-keyout $(BASE)/config/$(HAPROXY_NAME)/server.key \
		-out $(BASE)/config/$(HAPROXY_NAME)/server.crt \
		-subj "/C=UK/O=SKAO/OU=IT/CN=$(UI_HOST)"; \
	fi
	openssl x509 -noout -text -in $(BASE)/config/$(HAPROXY_NAME)/server.crt
	cat $(BASE)/config/$(HAPROXY_NAME)/server.crt \
		$(BASE)/config/$(HAPROXY_NAME)/server.key \
		> $(BASE)/config/$(HAPROXY_NAME)/server.pem
	ls -latr $(BASE)/config/$(HAPROXY_NAME)

haproxy:
	docker rm -f $(HAPROXY_NAME) || true
	sleep 1
	mkdir -p $(BASE)/config/$(HAPROXY_NAME)
	echo "$${HAPROXY_CONFIG}" | envsubst > $(BASE)/config/$(HAPROXY_NAME)/haproxy.cfg
	docker run --network consul --name $(HAPROXY_NAME) \
	$(HOST_LIST) \
	-p 8080:80 -p 443:443 \
	--ip $(HAPROXY_IP) \
	-v $(BASE)/config/$(HAPROXY_NAME)/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg \
	-v $(BASE)/config/$(HAPROXY_NAME)/server.pem:/usr/local/etc/haproxy/server.pem \
	-v $(BASE)/auth-request.lua:/usr/local/etc/haproxy/auth-request.lua \
	-v $(BASE)/json.lua:/usr/local/share/lua/5.3/json.lua \
	-v $(BASE)/haproxy-lua-http:/usr/share/haproxy/haproxy-lua-http \
	-d $(HAPROXY_IMAGE) -f /usr/local/etc/haproxy/haproxy.cfg; \


proxy: self-signed redis oauth2proxy haproxy

clean-proxy:
	docker rm -f $(HAPROXY_NAME) || true
	docker rm -f $(OAUTH2PROXY_NAME) || true
	docker rm -f $(REDIS_NAME) || true

